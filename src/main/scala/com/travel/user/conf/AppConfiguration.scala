package com.travel.user.conf

import akka.util.Timeout
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._

trait AppConfiguration {

  private val conf = ConfigFactory.load()

  def getHost = conf.getString("app.host")
  def getPort = conf.getInt("app.port")
  def getTimeout = Timeout(conf.getInt("app.timeout").seconds)

}
