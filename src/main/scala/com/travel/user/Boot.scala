package com.travel.user

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import com.travel.user.conf.AppConfiguration
import com.travel.user.service.UserServiceActor
import spray.can.Http

object Boot extends App with AppConfiguration {

  implicit val system = ActorSystem("user-system")
  val service = system.actorOf(Props[UserServiceActor], "user-service")

  implicit val timeout = getTimeout

  IO(Http) ? Http.Bind(service, interface = getHost, port = getPort)

}
