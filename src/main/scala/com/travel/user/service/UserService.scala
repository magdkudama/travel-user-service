package com.travel.user.service

import akka.actor.Actor
import spray.http.MediaTypes._
import spray.routing._

class UserServiceActor extends Actor with UserService {

  def actorRefFactory = context
  def receive = runRoute(userRoutes)

}

trait UserService extends HttpService {

  val userRoutes =
    path("") {
      get {
        respondWithMediaType(`application/json`) {
          complete {
            <html>
              <body>
                <h1>Say hello to <i>spray-routing</i> on <i>spray-can</i>!</h1>
              </body>
            </html>
          }
        }
      }
    }

}
